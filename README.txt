DESCRIPTION:

A linked-list is a sequence of data structures which are connected together via links.It is a sequence of links which contains items. 
Each link contains a connection to another link.They are a dynamic in nature which allocates the memory when required.

DIRECTORY STRUCTURE :

-source/implementation	: contains the source code of linkedlist implementation
-source/include			: contains the header file with the declarations necessary for the linkedlist implementation
-source/test			: contains the unit tests file to test the linkedlist implementation
-build-makefile			: contains the makefile necessary for the linkedlist implementation
-build-cmake			: contains the CMakeLists.txt necessary for the linkedlist implementation

TO RUN :

Makefile:

-redirect to the makefile directory
-in the cmd , type make followed by make static-lib to generate and link the static library to the executable and run
-in the cmd , type make followed by make dynamic-lib to generate and link the shared/dynamic library to the executable and run
-in the cmd , type make liball to generate both the libraries and execute using both libraries

CMake:

-redirect to the CMakeLists.txt directory
-in the cmd , type 
	cmake -DVARIABLE1:STRING=static
	make
	./test

 to generate and link the static library to the executable and run

-in the cmd , type 
	cmake -DVARIABLE1:STRING=shared 
	make
	./test
 to generate and link the shared library to the executable and run

